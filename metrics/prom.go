package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

const (
	Topic = "topic"
	Key   = "key"
)

var UserCountGauge = prometheus.NewGauge(prometheus.GaugeOpts{
	Name: "active_session",
	Help: "Number of Current User in session.",
})

var GuestCountGauge = prometheus.NewGauge(prometheus.GaugeOpts{
	Name: "guest_session",
	Help: "Number of Guest User in session.",
})


var SubscribeMessageCountVec = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "sub_msg_count",
		Help: "Incoming Message count by topic",
	},
	[]string{Topic, Key})

var PublishMessageCountVec = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "pub_msg_count",
		Help: "Outgoing Message count by topic",
	},
	[]string{Topic, Key})
