package server

import (
	"net"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

type TransitServer interface {
	Register(conn net.Conn, card *user.NationalCard, licenseReq *message.LicenseRequest) // Register session into transit system
	// Listen(chan []byte)                         // Listen to server to get Response
}
