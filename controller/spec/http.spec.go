package server

import "net/http"

type HTTPServer interface {
	UpgradeToWS(req *http.Request, res http.ResponseWriter)         // UpgradeToWS: Handle upgrade to Ws (and should upgrade to WS)
	HandleNormalRequest(req *http.Request, res http.ResponseWriter) // Handle any other API (if any)
	Serve()                                                         // Run Server
}
