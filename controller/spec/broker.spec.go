package server

import (
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
)

// Broker : transit & population message broker
type Broker interface {
	Publish(topic, key string, message []byte) error

	SubscribePopulation(receiver chan *baan.WorldPopulations)
	UnSubscribePopulation()

	SubscribeTransitResult(licenseID string, receiver chan *message.TransitResult)
	UnSubscribeTransitResult(licenseID string)

	WatchResultOn(topic string)
}
