package transitServer

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

type JSON map[string]interface{}

func TestWebSocket(t *testing.T) {
	var expect string
	var p []byte
	var err error

	license := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwMzEzMjM1MjEiLCJiYWFuIjoia29oIn0.N690EYoulLzxgzqKP7efGCLN2USVObLgeeLfuGj2yMo"            // id: 6031323521, baan: koh
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwMzEzMjM1MjEiLCJmYWN1bHR5IjoyLCJnZW5kZXIiOjB9.AaxJOunkraUfmq086mubhI-N5oDuK_Gu6P2YDZFKqsg" // faculty: 2 gender:0
	url := fmt.Sprintf("ws://localhost:8080/?license=%s&token=%s", license, token)

	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	// Send message to server, read response and check to see if it's what we expect.

	// Test 1: bad type
	if err = ws.WriteJSON(JSON{
		"a": "transix",
		"d": JSON{
			"to": "jo",
		},
	}); err != nil {
		t.Logf("Write Error %s\n", err)
		t.FailNow()
	}

	_, p, err = ws.ReadMessage()
	if err != nil {
		t.Logf("Read Error %s\n", err)
		t.FailNow()
	}

	expect = "Fuck Wrong type"
	if string(p) != expect {
		t.Logf("Bad type should reply with [%s] but got [%s]\n", expect, p)
		t.FailNow()
	}

	// Test 2: bad struct
	if err := ws.WriteMessage(websocket.TextMessage, []byte(`{"a":"transix", "d": {"to": "khoom"}}xxx`)); err != nil {
		t.Logf("Write Error %s\n", err)
		t.FailNow()
	}

	_, p, err = ws.ReadMessage()
	if err != nil {
		t.Logf("Read Error %s\n", err)
		t.FailNow()
	}

	expect = "Fuck bad struct"
	if string(p) != expect {
		t.Logf("Bad struct should reply with [%s] but got [%s]\n", expect, p)
		t.FailNow()
	}

	// Test 3: bad struct
	if err := ws.WriteMessage(websocket.TextMessage, []byte(`Hello world!`)); err != nil {
		t.Logf("Write Error %s\n", err)
		t.FailNow()
	}

	_, p, err = ws.ReadMessage()
	if err != nil {
		t.Logf("Read Error %s\n", err)
		t.FailNow()
	}

	expect = "Fuck bad struct"
	if string(p) != expect {
		t.Logf("Bad struct should reply with [%s] but got [%s]\n", expect, p)
		t.FailNow()
	}

	var res JSON
	// Transit to JO -- set baseline baan for next tests
	ws.WriteJSON(JSON{
		"a": "transit",
		"d": JSON{
			"to": "jo",
		},
	})
	// t.FailNow()
	ws.SetReadDeadline(time.Now().Add(7 * time.Second))
	_, p, err = ws.ReadMessage()
	err = json.Unmarshal(p, &res)
	if err != nil {
		t.Logf("Response Should be in JSON;")
		t.Logf("Error was %s\n", err)
		t.FailNow()
	}
	if reason, exists := res["reason"].(string); !exists {
		t.Logf("Response Should have field `reason`")
		t.FailNow()
	} else if reason == "OK" {
		t.Logf("[Notice] Reason OK, waiting next diff")
		err = ws.ReadJSON(&res)
		if err != nil {
			t.Logf("diff message should be JSON %s\n", err)
			t.FailNow()
		} else {
			t.Logf("OK Diff was %#v\n", res)
		}
	} else if reason == "Failed" {
		t.Logf("[Notice] Reason no Need to wait")
	}

	// move from jo(11) to preaw(25)
	ws.SetReadDeadline(time.Now().Add(7 * time.Second))
	t.Logf("- test move to same baan")
	ws.WriteJSON(JSON{
		"a": "transit",
		"d": JSON{
			"to": "preaw",
		},
	})
	res = nil
	err = ws.ReadJSON(&res)
	if err != nil {
		t.Logf("Read Error %s\n", err)
		t.FailNow()
	}
	if res["reason"] != "OK" {
		t.Logf("Moving Should success")
		t.FailNow()
	}
	// read diff
	var expectedJSON JSON
	temp := []byte(`{"11":{"c":-1,"f":[0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"g":[-1,0],"l":0},"25":{"c":1,"f":[0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"g":[1,0],"l":0},"address":"preaw"}`)
	json.Unmarshal(temp, &expectedJSON)
	res = make(JSON)
	err = ws.ReadJSON(&res)
	//

	if !reflect.DeepEqual(res["11"], expectedJSON["11"]) || !reflect.DeepEqual(res["25"], expectedJSON["25"]) {
		t.Logf("Diff isn't equal to expected JSON")
		t.Logf("Expected --%#v\n\n", expectedJSON)
		t.Logf("Actual --- %#v\n\n", res)
		t.FailNow()
	}
	t.Logf("- test move to other baan")
}
