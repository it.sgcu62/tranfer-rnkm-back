package transitServer

import (
	"crypto/sha256"
	"fmt"
	"math/rand"
	"net"
	"os"
	"time"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
	"github.com/pkg/errors"
)

var randomizer = rand.New(rand.NewSource(time.Now().UnixNano()))

// issueLicense crate new license when user is connected
func (s *MyTransitServer) issueLicense(
	conn net.Conn,
	card *user.NationalCard,
) *user.DrivingLicense {
	if card == nil {
		return &user.DrivingLicense{
			NationalCard: user.NationalCard{
				UserID:  "Guest",
				Faculty: 99,
				Gender:  99,
			},
			ID:         "",
			ClientConn: conn,
		}
	}
	newLicenseID := fmt.Sprintf("%x%f", sha256.Sum256([]byte(time.Now().Format(time.RFC3339Nano))), randomizer.Float64())
	newLicense := &user.DrivingLicense{
		NationalCard: user.NationalCard{
			UserID:  card.UserID,
			Faculty: card.Faculty,
			Gender:  card.Gender,
		},
		ID:         newLicenseID,
		ClientConn: conn,
	}
	if newLicense.Address == "" {
		// will insert mongo & redis query here
		baan, err := s.userRepo.GetCurrentAddress(newLicense.NationalCard.UserID)
		if err != nil || baan == "" {
			s.logger.SetOutput(os.Stderr)
			s.logger.Printf("Error Getting %s's Baan; %+v\n", newLicense.NationalCard.UserID, errors.WithStack(err))
			s.logger.SetOutput(os.Stdout)
			return nil
		}
		newLicense.Address = baan
	}
	s.userRepo.SetAllowedDrivingLicenseID(newLicense.NationalCard.UserID, newLicenseID)
	return newLicense
}

// isValidLicense: validate license's ID
func (s *MyTransitServer) isValidLicense(license *user.DrivingLicense) bool {
	validID, err := s.userRepo.GetAllowedDrivingLicenseID(license.NationalCard.UserID)
	if err != nil { // fail-safe
		return false
	}
	return validID == license.ID
}
