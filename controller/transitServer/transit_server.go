package transitServer

import (
	"log"
	"net"
	"os"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/adapter/population"

	server "github.com/it-sgcu/rnkm-transit/loadbalancer/controller/spec"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/repository"
)

// MyTransitServer : Transit server
type MyTransitServer struct {
	userRepo          repository.UserRepository
	broker            server.Broker
	populationAdapter *population.PopulationAdapter
	logger            *log.Logger
	popUpdate         chan *population.PopulationAdapter
}

// NewTransitServer : create instance of TransitServer
func NewTransitServer(userRepo repository.UserRepository, populationAdapter *population.PopulationAdapter, broker server.Broker) *MyTransitServer {
	s := &MyTransitServer{}
	s.userRepo = userRepo
	s.broker = broker
	s.populationAdapter = populationAdapter
	s.logger = log.New(os.Stdout, "[Websocket]", log.LstdFlags)
	s.popUpdate = make(chan *population.PopulationAdapter, 100)
	return s
}

// Register : Register user to Transit server
func (s *MyTransitServer) Register(conn net.Conn, card *user.NationalCard) {
	license := s.issueLicense(conn, card)
	if license == nil {
		conn.Close()
	} else if license.NationalCard.UserID == "Guest" {
		go s.GuestHandler(license)
	} else {
		go s.Handler(license)
	}
}
