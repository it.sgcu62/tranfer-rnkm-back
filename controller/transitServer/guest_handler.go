package transitServer

import (
	"encoding/json"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/adapter/population"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

func (s *MyTransitServer) GuestHandler(license *user.DrivingLicense) {

	popUpdateChan := make(chan *population.State, 10)

	s.populationAdapter.SubscribePopulation(popUpdateChan)
	metrics.GuestCountGauge.Inc()

	defer func() {
		s.populationAdapter.UnSubscribePopulation(popUpdateChan)
		metrics.GuestCountGauge.Dec()
		license.Terminate()
	}()
	incomingWSMessage := pipeWSMessage(license)

	for !license.IsTerminated() {
		select {
		case <-incomingWSMessage:
			allBaanPop := s.populationAdapter.CurrentState.LatestState()
			allBaanPop["t"] = "full"
			injectedJSON, _ := json.Marshal(allBaanPop)
			license.SafelyWriteClientMessage(injectedJSON)

		case incomingPopMessage := <-popUpdateChan:
			popJSON := incomingPopMessage.LatestState()
			popJSON["t"] = "diff"
			injectedMessage, _ := json.Marshal(popJSON)
			license.SafelyWriteClientMessage(injectedMessage)
		}
	}
}
