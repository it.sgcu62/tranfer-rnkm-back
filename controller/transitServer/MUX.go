package transitServer

import (
	"encoding/json"
	"os"
	"strconv"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

// clientMessageMUX handle client message from WS
func (s *MyTransitServer) clientMessageMUX(msg []byte, currentLicense *user.DrivingLicense) string {
	var request message.WSRequest
	if len(msg) > 200 { // ignore big spam message
		return ""
	}
	err := json.Unmarshal(msg, &request)
	if err != nil {
		s.logger.Printf("\nD: bad message %s : %s\n", msg, err.Error())
		currentLicense.SafelyWriteClientMessage([]byte("Fuck bad struct"))
		return ""
	}

	switch request.Action {

	case "transit":
		var transit message.WSTransitDetail
		convert(request.Detail, &transit)

		if _, exist := constant.MockBaanLimit[transit.To]; !exist { // 404 cases
			clientMessage := map[string]interface{}{
				"t":  "tr",
				"ad": transit.To,
				"rs": "404",
			}
			message, _ := json.Marshal(clientMessage)
			currentLicense.SafelyWriteClientMessage(message)
			return ""
		}
		// TODO verify transit
		s.broker.WatchResultOn(currentLicense.Address)
		str, _ := json.Marshal(message.TransitRequest{
			DrivingLicense: *currentLicense,
			To:             transit.To,
		})
		s.broker.Publish("tranReq", transit.To, str)

	case "correction":
		allBaanPop := s.populationAdapter.CurrentState.LatestState()
		allBaanPop["t"] = "full"
		allBaanPop["address"] = currentLicense.Address
		open, _ := strconv.Atoi(os.Getenv("OPEN_TIME"))
		allBaanPop["openTime"] = open
		close, _ := strconv.Atoi(os.Getenv("CLOSE_TIME"))
		allBaanPop["closeTime"] = close
		injectedJSON, _ := json.Marshal(allBaanPop)
		currentLicense.SafelyWriteClientMessage(injectedJSON)

	case "finalize":
		jwt, err := s.userRepo.Finalize(currentLicense, s.broker)
		clientMessage := map[string]interface{}{
			"t":     "finalize",
			"token": jwt,
			"err":   err,
		}
		message, _ := json.Marshal(clientMessage)
		currentLicense.SafelyWriteClientMessage(message)
		currentLicense.Terminate()

	default:
		currentLicense.SafelyWriteClientMessage([]byte("Fuck Wrong type"))
	}
	return request.Action
}
