package transitServer

import (
	"encoding/json"
	"time"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/adapter/population"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

func (s *MyTransitServer) Handler(license *user.DrivingLicense) {

	popUpdateChan := make(chan *population.State, 10)
	transitChan := make(chan *message.TransitResult, 10)

	s.populationAdapter.SubscribePopulation(popUpdateChan)
	s.broker.SubscribeTransitResult(license.ID, transitChan)
	metrics.UserCountGauge.Inc()
	s.logger.Printf("L-userid = %v\n", license)

	defer func() {
		s.populationAdapter.UnSubscribePopulation(popUpdateChan)
		s.broker.UnSubscribeTransitResult(license.ID)
		metrics.UserCountGauge.Dec()
		license.Terminate()
	}()
	var isBlockingTransaction = false
	// TODO : check for guest, if guest : assign incomingWSMessage = nil
	incomingWSMessage := pipeWSMessage(license)
	timeoutTicker := &time.Ticker{C: nil}

	for !license.IsTerminated() {
		var WSMessageChannel = incomingWSMessage
		if isBlockingTransaction {
			WSMessageChannel = nil
		}

		select {
		case data := <-WSMessageChannel:
			if s.isValidLicense(license) {
				if s.clientMessageMUX(data, license) == "transit" {
					// !! Wait for result message before handle next request
					timeoutTicker = time.NewTicker(20 * time.Second)
					isBlockingTransaction = true
				}
			} else {
				return
			}

		case incomingTransitMessage := <-transitChan:
			if incomingTransitMessage.Reason == "200" {
				license.Address = incomingTransitMessage.DrivingLicense.Address
			}
			clientMessage := map[string]interface{}{
				"t":  "tr",
				"ad": incomingTransitMessage.Address,
				"rs": incomingTransitMessage.Reason,
			}
			message, _ := json.Marshal(clientMessage)
			license.SafelyWriteClientMessage(message)
			isBlockingTransaction = false
			timeoutTicker.Stop()

		case <-timeoutTicker.C:
			println("Request time out.")
			timeoutTicker.Stop()
			return

		case incomingPopMessage := <-popUpdateChan:
			popJSON := incomingPopMessage.LatestState()
			popJSON["t"] = "diff"
			popJSON["address"] = license.Address // Inject Address
			injectedMessage, _ := json.Marshal(popJSON)
			license.SafelyWriteClientMessage(injectedMessage)
		}
	}
}
