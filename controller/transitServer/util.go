package transitServer

import (
	"encoding/json"
	"log"
	"os"

	"github.com/gobwas/ws/wsutil"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

// marshalJSON: return json as []byte, no error checking
func marshalJSON(v interface{}) []byte {
	str, _ := json.Marshal(v)
	return str
}

// convert from into `to`, to MUST be pointer
func convert(from interface{}, to interface{}) error {
	str, err := json.Marshal(from)
	if err != nil {
		return err
	}
	err = json.Unmarshal(str, &to)
	return err
}

func pipeWSMessage(license *user.DrivingLicense) <-chan []byte {
	bytesChannel := make(chan []byte)
	go func() {
		var buffer []byte
		var err error
		bottleNeck := 0
		for {
			buffer, err = wsutil.ReadClientText(license.ClientConn)
			if err != nil {
				log.SetOutput(os.Stderr)
				log.Printf(
					"[WS] user %s connection terminated : %s",
					license.NationalCard.UserID,
					err.Error(),
				)
				license.Terminate()
				log.SetOutput(os.Stdout)
				return
			}
			select {
			case bytesChannel <- buffer:
			default:
				bottleNeck++
				if bottleNeck > 100 {
					log.SetOutput(os.Stderr)
					log.Printf(
						"[WS] user %s connection terminated : System bottleneck or DDoS",
						license.NationalCard.UserID,
					)
					license.Terminate()
					log.SetOutput(os.Stdout)
				}
			}
			// bytesChannel <- b
		}
	}()
	return bytesChannel
}
