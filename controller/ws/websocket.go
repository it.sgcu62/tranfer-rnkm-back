package wscontroller

import (
	"net"
	"net/url"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/repository"

	"github.com/gobwas/ws"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

// UpgradeToWS : upgrade net.Conn to Websocket
func UpgradeToWS(conn net.Conn, repo repository.UserRepository) (*user.NationalCard, error) {
	nationalCard := (*user.NationalCard)(nil)

	WSUpgrader := ws.Upgrader{
		OnHost: func(host []byte) error {
			println(string(host))
			if string(host) == "api.transit.gamma.rubnongcu.life" ||
				string(host) == "api.transit.rubnongcu.life" ||
				string(host) == "localhost" {
				return nil
			}
			return ws.RejectConnectionError(ws.RejectionStatus(404))
		},
		OnRequest: func(uri []byte) error {
			value, err := url.Parse(string(uri))
			if err == nil {
				tokenQuery := value.Query().Get("token")
				if tokenQuery != "" {
					nationalCard, err = repository.JWTService.ParseNationalCard(tokenQuery, repo)
				}
			}
			return err
		},
	}
	_, err := WSUpgrader.Upgrade(conn)
	return nationalCard, err
}
