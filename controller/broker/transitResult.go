package broker

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/prometheus/client_golang/prometheus"

	message "github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
	"github.com/segmentio/kafka-go"
)

// regist consumer, deregist consumer is sub's/unsub's duty respectively
func (b *KafkaBroker) startWatchTransitResult() {
	for i := range b.transitResultConsumer {
		b.transitResultConsumer[i] = kafka.NewReader(kafka.ReaderConfig{
			Brokers:   b.addr,
			Topic:     "tranRes",
			Partition: i,
			MinBytes:  5, // our message is small and real time
			MaxBytes:  10e6,
		})
		b.transitResultSubscribeCount[i] = 0
		b.transitResultConsumerTrigger[i] = make(chan bool)
		go b.watchPartition(i) // start the watcher go routine
	}
}

// watchePartition: start go routine to watch partition `id`
func (b *KafkaBroker) watchPartition(i int) {
	partID := strconv.Itoa(i) // for use with prom
	for {
		var transitResult message.TransitResult
		count := b.transitResultSubscribeCount[i]
		if count > 0 {
			fmt.Printf("Consume on partition : %d\n", i)
			m, err := b.transitResultConsumer[i].ReadMessage(context.Background())
			if err != nil && err != io.EOF {
				b.logError("TransitConsumerError : ", err)
				b.transitResultConsumer[i] = kafka.NewReader(kafka.ReaderConfig{
					Brokers:   b.addr,
					Topic:     "tranRes",
					Partition: i,
					MinBytes:  5, // our message is small and real time
					MaxBytes:  10e6,
				})
				continue
			}
			metrics.SubscribeMessageCountVec.With(prometheus.Labels{
				metrics.Topic: "transitRes",
				metrics.Key:   partID,
			}).Inc()
			json.Unmarshal(m.Value, &transitResult)
			b.logger.Printf("[tranRes] <--- %s -> %s:%s...\n", transitResult.NationalCard.UserID, transitResult.Address, transitResult.Reason)
			b.transitResultMutex.Lock()
			if channel, exist := b.transitResultCallbacks[transitResult.DrivingLicense.ID]; exist {
				select {
				case channel <- &transitResult:
					b.UnWatch(i)
				default:
					b.logError("[Warning]: transitResult message overflowed", nil)
				}
			}
			b.transitResultMutex.Unlock()
		} else {
			<-b.transitResultConsumerTrigger[i]
		}
	}
}

func (b *KafkaBroker) SubscribeTransitResult(licenseID string, receiver chan *message.TransitResult) {
	b.transitResultMutex.Lock()
	b.transitResultCallbacks[licenseID] = receiver
	b.transitResultMutex.Unlock()
}

func (b *KafkaBroker) WatchResultOn(topic string) {
	targetPartition := b.balancer.Balance(
		kafka.Message{
			Key: []byte(topic),
		},
	)
	if b.transitResultSubscribeCount[targetPartition] == 0 {
		// Never subscribed, Seek offset to last offset - 1
		l, _ := b.transitResultConsumer[targetPartition].ReadLag(context.Background())
		b.transitResultConsumer[targetPartition].SetOffset(l - 1)
	}
	b.transitResultSubscribeCount[targetPartition]++

	// try to send signal to chan
	// if chan fulfilled, go on.
	select {
	case b.transitResultConsumerTrigger[targetPartition] <- true:
	default:
	}
}

func (b *KafkaBroker) UnWatch(partition int) {
	b.transitResultSubscribeCount[partition]--
}

func (b *KafkaBroker) UnSubscribeTransitResult(licenseID string) {
	b.transitResultMutex.Lock()
	delete(b.transitResultCallbacks, licenseID)
	b.transitResultMutex.Unlock()
}
