package broker

import (
	"context"
	"encoding/json"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/prometheus/client_golang/prometheus"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
)

func (b *KafkaBroker) startWatchPopulation() {
	l, _ := b.populationConsumer.ReadLag(context.Background())
	b.populationConsumer.SetOffset(l - 1)
	go func() {
		for {
			var newPopulation baan.WorldPopulations
			m, err := b.populationConsumer.ReadMessage(context.Background())
			if err != nil {
				b.logError("ERROR: read error on population", err)
				continue
			}
			metrics.SubscribeMessageCountVec.With(prometheus.Labels{
				metrics.Topic: "population",
				metrics.Key:   "population",
			}).Inc()
			b.logger.Printf("[population] <--- %s...\n", truncate(m.Value, 20))
			err = json.Unmarshal(m.Value, &newPopulation)
			if err != nil {
				b.logError("ERROR: unmarshal message to baan.WorldPopulations", err)
				continue
			}
			b.populationCallback <- &newPopulation
			b.populationConsumer.CommitMessages(context.Background(), m)
		}
	}()
}

func (b *KafkaBroker) SubscribePopulation(receiver chan *baan.WorldPopulations) {
	b.populationCallback = receiver
}

func (b *KafkaBroker) UnSubscribePopulation() {
	b.populationCallback = nil
}
