package broker

import (
	"context"
	"log"
	"os"
	"sync"
	"time"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/prometheus/client_golang/prometheus"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
	message "github.com/it-sgcu/rnkm-transit/loadbalancer/model/message"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
)

// KafkaBroker simple implementation of Kafka (no partiton yet)
type KafkaBroker struct {
	transitReqProducer *kafka.Writer

	populationConsumer          *kafka.Reader
	transitResultConsumer       []*kafka.Reader
	transitResultSubscribeCount []int

	populationCallback     chan *baan.WorldPopulations
	transitResultCallbacks map[string] /* licenseID */ chan *message.TransitResult

	transitResultConsumerTrigger []chan bool
	balancer                     kafka.Balancer
	logger                       *log.Logger
	transitResultMutex           *sync.Mutex
	addr                         []string
}

// NewKafkaBroker : create kafka broker
func NewKafkaBroker(address []string) *KafkaBroker {
	b := &KafkaBroker{
		transitResultConsumer:       make([]*kafka.Reader, len(constant.BaanList)),
		transitResultSubscribeCount: make([]int, len(constant.BaanList)),

		transitResultCallbacks: make(map[string]chan *message.TransitResult, 0),

		transitResultConsumerTrigger: make([]chan bool, 36),
		transitResultMutex:           &sync.Mutex{},
		addr:                         address,
		logger:                       log.New(os.Stdout, "[Broker]", log.LstdFlags),
		balancer:                     &constant.TransitBalancer{},
	}
	b.populationConsumer = kafka.NewReader(kafka.ReaderConfig{
		Brokers:   address,
		Topic:     "population",
		Partition: 0,
		MinBytes:  5,
		MaxBytes:  10e6,
	})
	b.transitReqProducer = kafka.NewWriter(kafka.WriterConfig{
		Brokers:      address,
		Topic:        "tranReq",
		Balancer:     b.balancer,
		RequiredAcks: 1,
		BatchTimeout: 10 * time.Millisecond,
	})
	return b
}

func truncate(s []byte, length int) []byte {
	if len(s) < length {
		return s
	}
	return s[:length]
}

func (b *KafkaBroker) logError(msg string, err error) {
	b.logger.SetOutput(os.Stderr)
	b.logger.Println(msg)
	if err != nil {
		b.logger.Printf("%+v\n", errors.WithStack(err))
	}
	b.logger.SetOutput(os.Stdout)
}

// Start : Start Writer/Reader loop; should be called when sub's are ready so they doesn't miss message
func (b *KafkaBroker) Start() {
	b.startWatchPopulation()
	b.startWatchTransitResult()
}

// Stop : Close all writers, consumer
func (b *KafkaBroker) Stop() {
	b.populationConsumer.Close()
	for _, consumer := range b.transitResultConsumer {
		consumer.Close()
	}
}

// Publish : publish message with value: `value` in to topic `topic` (I might add key in future ?)
func (b *KafkaBroker) Publish(topic, key string, value []byte) error {
	if topic != "tranReq" || topic != "finalize" {
		b.logError("Only topic 'tranReq' or 'finalize' is allowed", nil)
		return errors.New("Only topic 'tranReq' or 'finalize' is allowed")
	}
	// Prometheus Metric
	metrics.PublishMessageCountVec.With(prometheus.Labels{
		metrics.Topic: "transitReq",
		metrics.Key:   key,
	}).Inc()
	b.logger.Printf("[%s] ---> %s... \n", topic, value)
	// b.logger.Printf("[tranRes] <--- %s -> %s:%s...\n", transitResult.NationalCard.UserID, transitResult.Address, transitResult.Reason)
	return b.transitReqProducer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(key),
			Value: value,
		},
	)
}
