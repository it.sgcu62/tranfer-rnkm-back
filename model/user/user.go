package user

import (
	"errors"
	"fmt"
	"net"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

type Mephitcha struct {
	From      string `json:"from"`
	To        string `json:"to"`
	StudentID string `json:"studentId"`
}

// Valid: validate Mephitcha data, implements jwt.Claims
func (m *Mephitcha) Valid() error {
	// TODO: this is just checking empty, should nbe more somprehensive check
	if m.From != "" && m.To != "" && m.StudentID != "" {
		return nil
	}
	return errors.New("Invalid NationalCard")
}

// NationalCard : Freshmen student info
type NationalCard struct {
	UserID  string `json:"id"`
	Faculty int    `json:"faculty"`
	Gender  int    `json:"gender"`
	Secret  []byte `json:"-"`
}

// Valid: validate nationalCard data, implements jwt.Claims
func (card *NationalCard) Valid() error {
	// TODO: this is just checking empty, should nbe more somprehensive check
	if card.UserID != "" {
		return nil
	}
	return errors.New("Invalid NationalCard")
}

// DrivingLicense : Transit session
type DrivingLicense struct {
	NationalCard `json:"nationality"`
	ID           string   `json:"id"` // randomized for ensure that session is unique
	Address      string   `json:"addr"`
	ClientConn   net.Conn `json:"-"`
	terminate    bool
}

// Terminate license client connection, DO NOT destroy by change license ID to ""
// because licenseID might be used to close kafka connection
func (license *DrivingLicense) Terminate() {
	fmt.Printf("Terminate license for user:%s\n", license.UserID)
	license.ClientConn.Close()
	license.terminate = true
}

// IsTerminated return is license terminated
func (license *DrivingLicense) IsTerminated() bool {
	return license.terminate
}

// SafelyWriteClientMessage return is license terminated
func (license *DrivingLicense) SafelyWriteClientMessage(msg []byte) bool {
	if wsutil.WriteServerMessage(license.ClientConn, ws.OpText, msg) != nil {
		license.Terminate()
	}
	return license.terminate
}
