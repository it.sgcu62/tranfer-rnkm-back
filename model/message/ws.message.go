package message

// Define message struct for WS payloads

// WSRequest Base for any WS Request
type WSRequest struct {
	Action string      `json:"a"`
	Detail map[string]interface{} `json:"d"` // detail varies by action
}

// WSTransitDetail transit baan request
type WSTransitDetail struct {
	To string `json:"to"`
}
