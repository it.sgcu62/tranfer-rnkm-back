package message

import (
	user "github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

// TransitRequest : Request message for transport to another baan
type TransitRequest struct {
	user.DrivingLicense `json:"license"`
	To                  string `json:"to"`
}

// TransitResult : Response message of transport result
type TransitResult struct {
	user.DrivingLicense `json:"license"`
	Reason              string `json:"reason"`
}
