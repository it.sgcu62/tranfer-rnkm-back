package message

import "errors"

// LicenseRequest : jwt that's sent in /?src=xxxxxxxx
type LicenseRequest struct {
	StudentID string `json:"id"`
	Address   string `json:"baan"`
}

// Valid : validate data in struct, implements jwt.Claims
func (req *LicenseRequest) Valid() error {
	if req.StudentID != "" && req.Address != "" {
		return nil
	}
	return errors.New("Invalid LicenseRequest")
}
