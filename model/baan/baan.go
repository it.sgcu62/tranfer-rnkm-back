package baan

import (
	"encoding/json"

	constant "github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
)

// Population : Population of baan
type Population struct {
	Limit   int    `json:"l"`
	Current int    `json:"c"`
	Faculty []int  `json:"f"`
	Gender  [2]int `json:"g"`
}

// WorldPopulations : Population for each baan
type WorldPopulations map[string]Population

// IsAllZero : Check is WorldPopulations is all zero
func (worldPopulation WorldPopulations) IsAllZero() bool {
	isAllZero := true
	for _, baan := range constant.BaanList {
		if !isAllZero {
			break
		}
		baanPopulation := worldPopulation[baan]
		isAllZero = isAllZero &&
			(baanPopulation.Limit == 0) &&
			(baanPopulation.Current == 0) &&
			(baanPopulation.Gender[0] == 0) &&
			(baanPopulation.Gender[1] == 0)
		for _, facultyCount := range baanPopulation.Faculty {
			isAllZero = isAllZero && facultyCount == 0
		}
	}
	return isAllZero
}

// MarshalJSON (WorldPopulations) : marshal WorldPopulations tp JSON
func (worldPopulation WorldPopulations) MarshalJSON() ([]byte, error) {
	if worldPopulation.IsAllZero() {
		return []byte(""), nil
	}
	output := map[int]interface{}{}
	for i, baan := range constant.BaanList {
		baanPopulation := worldPopulation[baan]
		isNilBaan :=
			(baanPopulation.Limit == 0) &&
				(baanPopulation.Current == 0) &&
				(baanPopulation.Gender[0] == 0) &&
				(baanPopulation.Gender[1] == 0)

		for _, facultyCount := range baanPopulation.Faculty {
			isNilBaan = isNilBaan && facultyCount == 0
		}
		if !isNilBaan {
			output[i] = baanPopulation
		}
	}
	return json.Marshal(output)
}

// UnmarshalJSON : unmarshal JSON to WorldPopulations
func (worldPopulation *WorldPopulations) UnmarshalJSON(data []byte) error {
	var mapInt map[int]Population
	err := json.Unmarshal(data, &mapInt)
	if err != nil {
		return err
	}
	pop := make(WorldPopulations)
	for i, baan := range constant.BaanList {
		pop[baan] = mapInt[i]
	}
	*worldPopulation = pop
	return nil
}
