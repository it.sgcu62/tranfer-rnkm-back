package population

import (
	"log"
	"os"

	"github.com/pkg/errors"

	"github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
	baanModel "github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
)

// PopulationAdapter : subscribe for `worldPopulation` topic and publish `diff` to `population` topic
type PopulationAdapter struct {
	Receiver            chan *baan.WorldPopulations
	populationCallbacks []chan *State

	CurrentState *State
	logger       *log.Logger
}

// NewAdapter : Create new population adpter
func NewAdapter() *PopulationAdapter {
	facultyCount := len(constant.FacultyList)
	newAdapter := &PopulationAdapter{
		Receiver: make(chan *baan.WorldPopulations, 10),
		logger:   log.New(os.Stdout, "[Population Adapter]", log.LstdFlags),
		CurrentState: &State{
			baanState: make(baan.WorldPopulations),
			commitNo:  0,
		},
	}
	for baan, limit := range constant.MockBaanLimit {
		newAdapter.CurrentState.baanState[baan] = baanModel.Population{
			Limit:   limit,
			Current: 0,
			Faculty: make([]int, facultyCount),
			Gender:  [2]int{0, 0},
		}
	}
	return newAdapter
}

func (b *PopulationAdapter) logError(msg string, err error) {
	b.logger.SetOutput(os.Stderr)
	b.logger.Println(msg)
	if err != nil {
		b.logger.Printf("%+v\n", errors.WithStack(err))
	}
	b.logger.SetOutput(os.Stdout)
}

func (adapter *PopulationAdapter) Start() {
	for {
		diff := adapter.CurrentState.SetState(<-adapter.Receiver)
		if diff != nil {
			for _, listener := range adapter.populationCallbacks {
				select {
				case listener <- diff:
				default:
					adapter.logError("[Warning]: population message overflowed", nil)
				}
			}
		}
	}
}

func (adapter *PopulationAdapter) SubscribePopulation(receiver chan *State) {
	adapter.populationCallbacks = append(adapter.populationCallbacks, receiver)
}

func (adapter *PopulationAdapter) UnSubscribePopulation(receiver chan *State) error {
	defer func() {
		if r := recover(); r != nil {
			println("Recovered @UnSubscribePopulation")
		}
	}()
	cbLen := len(adapter.populationCallbacks)
	err := errors.New("receiver not found")
	for i, receiverChan := range adapter.populationCallbacks {
		if receiverChan == receiver {
			adapter.populationCallbacks[i], adapter.populationCallbacks[cbLen-1] = adapter.populationCallbacks[cbLen-1], adapter.populationCallbacks[i]
			adapter.populationCallbacks = adapter.populationCallbacks[:cbLen-1]
			err = nil
			break
		}
	}
	if err != nil {
		adapter.logError("UnSubscribePopulation error :", err)
	}
	return err
}
