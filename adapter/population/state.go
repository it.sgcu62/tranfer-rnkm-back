package population

import baanModel "github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"

type State struct {
	baanState baanModel.WorldPopulations
	commitNo  int
}

// SetState (*Watcher): Set new baan population status, return JSON form of change between old & new state
func (s *State) SetState(newPopulation *baanModel.WorldPopulations) *State {
	diff := make(baanModel.WorldPopulations)
	for baan, oldBaanPopulation := range s.baanState {
		newBaanPopulation := (*newPopulation)[baan]
		facultyDiff := make([]int, len(oldBaanPopulation.Faculty))
		for i, v := range newBaanPopulation.Faculty {
			facultyDiff[i] = v - oldBaanPopulation.Faculty[i]
		}
		diff[baan] = baanModel.Population{
			Limit:   newBaanPopulation.Limit - oldBaanPopulation.Limit,
			Current: newBaanPopulation.Current - oldBaanPopulation.Current,
			Faculty: facultyDiff,
			Gender: [2]int{
				newBaanPopulation.Gender[0] - oldBaanPopulation.Gender[0],
				newBaanPopulation.Gender[1] - oldBaanPopulation.Gender[1],
			},
		}
	}
	s.baanState = *newPopulation
	if !diff.IsAllZero() {
		s.commitNo++
		return &State{
			commitNo:  s.commitNo,
			baanState: diff,
		}
	}
	return nil
}

// LatestState : get latest world population state
func (s *State) LatestState() map[string]interface{} {
	return map[string]interface{}{
		"cn": s.commitNo,
		"s":  s.baanState,
	}
}
