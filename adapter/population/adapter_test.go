package population

// import (
// 	"encoding/json"
// 	"reflect"
// 	"testing"

// 	"github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
// 	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/baan"
// )

// func emptyPopulation() baan.Population {
// 	return baan.Population{
// 		Limit:   100,
// 		Current: 0,
// 		Faculty: make([]int, len(constant.FacultyList)),
// 		Gender:  [2]int{},
// 	}
// }

// func newWorldPopulation() baan.WorldPopulations {
// 	pop := make(baan.WorldPopulations)
// 	for _, baan := range constant.BaanList {
// 		pop[baan] = emptyPopulation()
// 	}
// 	return pop
// }

// // bindAssert create assert function
// func bindAssert(t *testing.T) func(value bool) {
// 	return func(value bool) {
// 		if !value {
// 			t.FailNow()
// 		}
// 	}
// }

// func bindAssertEqual(t *testing.T) func(expected, actual interface{}) {
// 	return func(expected, actual interface{}) {
// 		if !reflect.DeepEqual(expected, actual) {
// 			t.Logf("Assert Failed:\nexpect = %#v\nactual = %#v\n", expected, actual)
// 			t.FailNow()
// 		}
// 	}
// }
// func TestSamePopulation(t *testing.T) {
// 	pop := newWorldPopulation()

// 	differ := NewDiffer()
// 	differ.SetState(pop)
// 	diff := differ.SetState(pop)

// 	assert := bindAssert(t)
// 	assert(len(diff) == 0)
// }

// func TestDifference(t *testing.T) {
// 	differ := NewDiffer()
// 	assertEq := bindAssertEqual(t)
// 	pop := newWorldPopulation()
// 	differ.SetState(pop)
// 	// t.Logf("Current state = %s\n", differ.LatestState())
// 	// change jo population
// 	newPopulation := newWorldPopulation()
// 	joPopulation := emptyPopulation()
// 	joPopulation.Limit += 150
// 	joPopulation.Current += 50
// 	joPopulation.Faculty[0] = 25
// 	joPopulation.Faculty[7] = 25
// 	newPopulation["jo"] = joPopulation

// 	joDiff := joPopulation
// 	joDiff.Limit = 150
// 	expectedDiff := make(baan.WorldPopulations)
// 	expectedDiff["jo"] = joDiff

// 	var diff baan.WorldPopulations
// 	_diff := differ.SetState(newPopulation)
// 	err := json.Unmarshal(_diff, &diff)
// 	if err != nil {
// 		t.Logf("Return value isn't json, it's [%s]\n", _diff)
// 		t.FailNow()
// 	}
// 	// assertEq(len(expectedDiff), len(diff))
// 	for k, _ := range expectedDiff {
// 		assertEq(expectedDiff[k], diff[k])
// 	}
// }
