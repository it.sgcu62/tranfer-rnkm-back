package constant

import (
	"github.com/kelseyhightower/envconfig"
)

type Configuration struct {
	RedisHost    string   `default:"redis:6379" split_words:"true"`
	KafkaBrokers []string `default:"kafka:9092" split_words:"true"`
	MongoURL     string   `default:"mongodb://mongo:27017" split_words:"true"`
	TokenSecret  []byte   `default:"LjHppeXR4eQAVw8x2BLGuNUHfyW53m7q" split_words:"true"`
	Debug        bool     `defaut:"false"`
}

var Config Configuration

func LoadConfig() {
	envconfig.Process("LOADBALANCER", &Config)
}
