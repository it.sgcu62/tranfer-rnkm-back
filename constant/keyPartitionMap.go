package constant

import (
	"github.com/segmentio/kafka-go"
)

var KeyPartitionMap = map[string]int{
	// population topic key map
	"population": 0,
	// finalize topic key map
	"finalize": 0,
	// tranReq/tranRes topic key map
	"aaum":            0,
	"abnormal":        1,
	"agape":           2,
	"buchayun":        3,
	"buem":            4,
	"dork":            5,
	"duidui":          6,
	"dung":            7,
	"fyo":             8,
	"indiana":         9,
	"indy":            10,
	"jo":              11,
	"jodehhuesa":      12,
	"judson":          13,
	"khunnoo":         14,
	"kids":            15,
	"koh":             16,
	"koom":            17,
	"laijai":          18,
	"mheenoi":         19,
	"nhai":            20,
	"paktak_agard":    21,
	"panarak":         22,
	"phee":            23,
	"por":             24,
	"preaw":           25,
	"rang":            26,
	"rhoy":            27,
	"seiyw":           28,
	"sod":             29,
	"soeiteelheemouy": 30,
	"tem":             31,
	"wang":            32,
	"wanted":          33,
	"work":            34,
	"yim":             35,
}

// TransitBalancer : transit kafka key to partition mappers
type TransitBalancer struct {
	*kafka.LeastBytes
}

// Balance : partition selector from message key
func (tb *TransitBalancer) Balance(msg kafka.Message, partitions ...int) int {
	if partition, exist := KeyPartitionMap[string(msg.Key)]; exist {
		return partition
	}
	return 0
}
