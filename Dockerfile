FROM golang:1.12
LABEL maintainer ["Tiger T. Pisnupoomi", "Roadchananat Khunakornophat"]
WORKDIR /go/src/github.com/it-sgcu/rnkm-transit/loadbalancer
COPY . .
RUN go get ./...
RUN go build
CMD ["./loadbalancer"]
EXPOSE 8000 8080
