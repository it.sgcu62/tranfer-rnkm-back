package main

import (
	"log"
	"net"
	"net/http"
	"os"

	"github.com/go-redis/redis"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/adapter/population"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/controller/broker"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/controller/transitServer"
	wscontroller "github.com/it-sgcu/rnkm-transit/loadbalancer/controller/ws"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/metrics"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/repository"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func init() {
	prometheus.MustRegister(metrics.UserCountGauge)
	prometheus.MustRegister(metrics.SubscribeMessageCountVec)
	prometheus.MustRegister(metrics.PublishMessageCountVec)
}

func main() {
	constant.LoadConfig()
	logger := log.New(os.Stdout, "[ATC]", log.LstdFlags)
	// broker := &controller.FakeBroker{}
	// broker := controller.NewKafkaBroker("localhost:8082", []string{"test"})

	populationAdapter := population.NewAdapter()
	go populationAdapter.Start()

	broker := broker.NewKafkaBroker(constant.Config.KafkaBrokers)
	broker.SubscribePopulation(populationAdapter.Receiver)
	broker.Start() // start broker and listen, note: if message arrive before `Subscribe` to broker, you will not get message when sub
	defer broker.Stop()

	// connect userrepo to super repo
	userRepo := repository.NewUserRepo(&redis.Options{
		Addr:     constant.Config.RedisHost,
		Password: "",
		DB:       0,
	})

	transitServer := transitServer.NewTransitServer(userRepo, populationAdapter, broker)

	http.Handle("/metrics", promhttp.Handler()) // register prom metric handler

	go http.ListenAndServe(":8000", nil)
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		logger.Fatal("Failed, " + err.Error())
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			logger.Printf("\nAccept new connection failed : " + err.Error())
			continue
		}
		card, err := wscontroller.UpgradeToWS(conn, userRepo)
		if err == nil {
			transitServer.Register(conn, card)
		} else {
			println(err.Error())
		}
	}
}
