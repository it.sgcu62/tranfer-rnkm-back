package repository

import "github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"

// UserRepository = redis or mongo, we get user's current baan, license ID here
type UserRepository interface {
	GetCurrentAddress(userID string) (string, error)
	GetAllowedDrivingLicenseID(userID string) (string, error)
	SetAllowedDrivingLicenseID(userID string, licenseID string) error
	Finalize(license *user.DrivingLicense) (evidence string, err error)
}
