package repository

import (
	"os"

	"github.com/go-redis/redis"
)

func SetStudentBaan(pipe redis.Pipeliner, id, baan string) error {
	_, err := pipe.HSet("student/"+id, "baan", baan).Result()
	return err
}

func (r *RealUserRepo) GetRedisBaan(userID string) (string, error) {
	baan, err := r.client.HGet("student/"+userID, "baan").Result()
	if err != nil {
		r.logger.SetOutput(os.Stderr)
		r.logger.Printf("Error Getting Baan of %s: %s\n", userID, err)
		r.logger.SetOutput(os.Stdout)
		return "", err
	}
	return baan, nil
}

func (r *RealUserRepo) GetAllowedDrivingLicenseID(userID string) (string, error) {
	sessionID, err := r.client.HGet("student/"+userID, "session").Result()
	if err != nil {
		r.logger.SetOutput(os.Stderr)
		r.logger.Printf("Error Getting LicenseID (session) of %s: %s\n", userID, err)
		r.logger.SetOutput(os.Stdout)
		return "", err
	}
	return sessionID, nil
}
func (r *RealUserRepo) SetAllowedDrivingLicenseID(userID string, licenseID string) error {
	_, err := r.client.HSet("student/"+userID, "session", licenseID).Result()
	if err != nil {
		r.logger.SetOutput(os.Stderr)
		r.logger.Printf("Error Setting LicenseID (session) of %s: %s\n", userID, err)
		r.logger.SetOutput(os.Stdout)
		return err
	}
	return nil
}
