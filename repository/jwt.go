package repository

import (
	"errors"

	"github.com/dgrijalva/jwt-go"
	constant "github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
)

var JWTService = MyJWTService{}

type MyJWTService struct{}

// ParseNationalCard : Parse jwt token to user.NationalCard struct
func (j *MyJWTService) ParseNationalCard(jwtString string, repo UserRepository) (*user.NationalCard, error) {
	token, err := jwt.ParseWithClaims(
		jwtString,
		&user.NationalCard{},
		func(t *jwt.Token) (interface{}, error) {
			return constant.Config.TokenSecret, nil
		})
	if err != nil {
		return nil, errors.New("Malformed UserNationalCard when parsing jwt" + err.Error())
	}
	card, ok := token.Claims.(*user.NationalCard)
	if !ok {
		return nil, errors.New("Malformed UserNationalCard when assert type (*user.NationalCard)")
	}
	return card, nil
}

// IssueEvidence : Issue user's transaction
func (j *MyJWTService) IssueEvidence(license *user.DrivingLicense, ancestry string) string {
	signedString, _ := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		&user.Mephitcha{
			StudentID: license.NationalCard.UserID,
			From:      ancestry,
			To:        license.Address,
		},
	).SignedString(constant.Config.TokenSecret)
	return signedString
}
