package repository

import (
	"context"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"github.com/go-redis/redis"
	constant "github.com/it-sgcu/rnkm-transit/loadbalancer/constant"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const STRING_WIDTH = 3 // Width of enconding in parseStringArray and encodeStringArray

type RealUserRepo struct {
	client *redis.Client
	logger *log.Logger
	Mongo  *mongo.Database
}

var _ UserRepository = &RealUserRepo{}

// NewUserRepo create new redis client // TODO: connect to mongo ?
func NewUserRepo(rOpt *redis.Options) *RealUserRepo {
	r := &RealUserRepo{
		logger: log.New(os.Stdout, "[Population]", log.Lshortfile),
	}
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	r.client = redis.NewClient(rOpt)

	// r.client = redis.NewFailoverClient(&redis.FailoverOptions{
	// 	MasterName:    "mymaster",
	// 	SentinelAddrs: []string{r.options.Addr},
	// })

	if _, err := r.client.Ping().Result(); err != nil {
		r.logger.Fatalf("error: %v\n", err)
	}
	mongoConn, err := mongo.Connect(ctx, options.Client().ApplyURI(constant.Config.MongoURL))
	if err != nil {
		log.Fatal(err)
	}
	r.Mongo = mongoConn.Database("freshmen", &options.DatabaseOptions{
		ReadPreference: readpref.SecondaryPreferred(),
	})
	return r
}

func (r *RealUserRepo) logError(msg string, err error) {
	r.logger.SetOutput(os.Stderr)
	r.logger.Println(msg)
	if err != nil {
		r.logger.Printf("%+v\n", errors.WithStack(err))
	}
	r.logger.SetOutput(os.Stdout)
}

func (r *RealUserRepo) GetCurrentAddress(userID string) (string, error) {
	var redisAddress string
	var err error
	if redisAddress, err = r.GetRedisBaan(userID); redisAddress == "" {
		return r.GetMongoBaan(userID), nil
	}
	return redisAddress, err
}
