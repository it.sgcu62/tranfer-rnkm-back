package repository

import (
	"context"
	"encoding/json"
	"time"

	server "github.com/it-sgcu/rnkm-transit/loadbalancer/controller/spec"
	"github.com/it-sgcu/rnkm-transit/loadbalancer/model/user"
	"go.mongodb.org/mongo-driver/bson"
)

func (r *RealUserRepo) GetSecret(userID string) []byte {
	var result bson.M
	ctx, _ := context.WithTimeout(context.Background(), 1*time.Second)
	collections := r.Mongo.Collection("Secret")
	res := collections.FindOne(ctx, bson.M{"_id": userID})
	err := res.Decode(&result)
	if err != nil {
		r.logError("Decode error", err)
		return []byte{}
	}
	return []byte(result["secret"].(string))
}

func (r *RealUserRepo) GetMongoBaan(userID string) string {
	var result bson.M
	ctx, _ := context.WithTimeout(context.Background(), 1*time.Second)
	collections := r.Mongo.Collection("Student")
	res := collections.FindOne(ctx, bson.M{
		"_id":          userID,
		"boardingPass": bson.M{"$exists": true, "$ne": nil},
	})
	err := res.Decode(&result)
	if err != nil {
		r.logError("Decode error", err)
		return ""
	}
	if baan, ok := result["baan"].(string); ok {
		return baan
	}
	return ""
}

// InsertRunCardTask insert "RunCard" task with type = "transit" and auto timestamp
func (r *RealUserRepo) InsertRunCardTask(userID string, finalBaan string) {
	r.Mongo.Collection("NameTag").InsertOne(context.Background(), bson.M{
		"_id":       userID,
		"type":      "transit",
		"baan":      finalBaan,
		"timestamp": time.Now(),
	})
}

// Finalize confirms transit of user. it invalidates license, writes to mongo, and publishes message to broker.
func (r *RealUserRepo) Finalize(license *user.DrivingLicense, broker server.Broker) (evidence string, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 1*time.Second)
	_, err = r.client.Del("student/" + license.NationalCard.UserID).Result()
	if err != nil {
		return "", err
	}
	_, err = r.Mongo.Collection("Student").UpdateOne(
		ctx,
		bson.M{"_id": license.NationalCard.UserID},
		bson.M{
			"$set": bson.M{
				"baan": license.Address,
			},
		},
	)
	ancestor := r.GetMongoBaan(license.NationalCard.UserID)
	str, _ := json.Marshal(map[string]interface{}{
		"id":       license.NationalCard.UserID,
		"ancestor": ancestor,
		"to":       license.Address,
	})
	broker.Publish("finalize", "finalize", str)
	if err != nil {
		return "", err
	}
	return JWTService.IssueEvidence(
		license,
		ancestor,
	), nil

	// Issue JWT

	// ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// collections := r.Mongo.Collection("Secret")
	// cur, err := collections.Find(ctx, bson.M{"_id": userID})
	// if err != nil {
	// 	r.logError("Get secret for "+userID+" error:", err)
	// }
	// defer cur.Close(ctx)
	// for cur.Next(ctx) {
	// 	var result bson.M
	// 	if cur.Decode(&result) != nil {
	// 		r.logError("Decode ", err)
	// 	}
	// 	return []byte(result["secret"].(string))
	// }
	// return []byte("")
}
